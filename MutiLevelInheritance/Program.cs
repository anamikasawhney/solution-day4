﻿namespace MutiLevelInheritance
{
    // calculate total result of students
    // there is student class > rn name batch
    // test > marks in 5 subjects, total_marks
    // sports > sportsName, score 
    // result > here we need to caluclate total result =   total_marks + score

    class Student
    {
        int rn;
        string name, batch;
        public void GetDetails()
        {
            Console.WriteLine("Enter rn");
            rn = byte.Parse(Console.ReadLine());
            Console.WriteLine("Enter name");
            name = Console.ReadLine();
            Console.WriteLine("Enter batch");
            batch = Console.ReadLine();
        }
        public void DisplayDetails()
        {
            Console.WriteLine($"RN is {rn}");
            Console.WriteLine($"Name is {name}");
            Console.WriteLine($"Batch is {batch}");
        }
    }
    class Test : Student
    {
        int[] marks = new int[] { 40, 89, 78, 90, 95 };
        protected int totalMarks;
        public void CalculateTotalMarks()
        {
            base.GetDetails();
            foreach(int mark in marks ) {  
            totalMarks += mark;
            }
        }
        public void DisplayDetails()
           
        {
            base.DisplayDetails();
            Console.WriteLine($"totalMarks are {totalMarks}");
        
        }
    }

    class Sports : Test
    {
        string sportsName;
       protected  int score;
        public void GetDetails()
        {base.CalculateTotalMarks();    
            Console.WriteLine("enter sportsName");
            sportsName = Console.ReadLine();
            Console.WriteLine("enter score");
            score = byte.Parse(Console.ReadLine());
        }
        public void DisplayDetails()
        {
            base.DisplayDetails();
            Console.WriteLine("sportsName is "  + sportsName);
            Console.WriteLine("score is " + score);
        }

    }

    class Result: Sports
    {
        int totalScore;
        public void CalculateTotalScore()
        {
            base.GetDetails();
            totalScore = totalMarks + score;
         }
        public void DisplayDetails()
        {
            base.DisplayDetails();
            Console.WriteLine("Total sciore is " + totalScore);
        }
        

    }

    internal class Program
    {
        static void Main(string[] args)
        {
         
        Result result = new Result();
            //result.score =
          
            result.CalculateTotalScore();
            result.DisplayDetails();
        }
    }
}
