﻿using System.ComponentModel.DataAnnotations;

namespace InheritanceDemo
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("PartTimeEmp");
            PartTimeEmployee partTimeEmployee = new PartTimeEmployee();
            partTimeEmployee.GetDetails();
            //partTimeEmployee.PartTimeGetDetails();
            partTimeEmployee.DisplayDetails();
            //partTimeEmployee.PartTimeDisplayDetails();

            Console.WriteLine("FullTimeEmp");
            FullTimeEmployee fullTimeEmployee = new FullTimeEmployee();
            fullTimeEmployee.GetDetails();
           //fullTimeEmployee.FullTimeGetDetails();
            fullTimeEmployee.DisplayDetails();
            //fullTimeEmployee.FullTimeDisplayDetails();
        }
    }
}
