﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.Marshalling;
using System.Text;
using System.Threading.Tasks;

namespace InheritanceDemo
{
    // EMployee are of 2 types
    // PartTime Employee > ContractDuration , Project, ChargesperDay 
    // FullTime Employee > Dept, Manager, BasicSalary, Doj
    internal class Employee
    {
        int id;
        string name;
        string address;
        public void GetDetails()
        {
            Console.WriteLine("Enter id");
            id = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Enter name");
            name = Console.ReadLine();
            Console.WriteLine("Enter Address");
            address = Console.ReadLine();
        }
        public void DisplayDetails()
        {
            Console.WriteLine($"Id is {id}");
            Console.WriteLine($"Name is {name}");
            Console.WriteLine($"Address is {address}");
        }
    }
}
