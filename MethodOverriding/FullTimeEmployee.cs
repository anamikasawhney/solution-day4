﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InheritanceDemo
{
    internal class FullTimeEmployee : Employee
    {
        string dept, manager;
        int basicSalary;
        DateTime doj;
        public override void GetDetails()
        //public void FullTimeGetDetails()
        {
            base.GetDetails();
            Console.WriteLine("Enter dept");
            dept = Console.ReadLine();
            Console.WriteLine("enter manager");
            manager = Console.ReadLine();
            Console.WriteLine("Enter basicSalary");
            basicSalary = Int32.Parse(Console.ReadLine());
            Console.WriteLine("eneter doj");
            doj = Convert.ToDateTime(Console.ReadLine());
        }
        //public void FullTimeDisplayDetails()
        public override void DisplayDetails()
        {
            base.DisplayDetails();
            Console.WriteLine($"dept is {dept}");
            Console.WriteLine($"manager is {manager}");
            Console.WriteLine($"basicSalary is {basicSalary}");
            Console.WriteLine($"doj is {doj}");
        }
    }
}

