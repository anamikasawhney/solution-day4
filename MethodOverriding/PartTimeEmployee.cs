﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InheritanceDemo
{
    internal class PartTimeEmployee : Employee
    {
        string contractDuration, project;
        int chargesPerDay;
        //public void PartTimeGetDetails()
        public override void GetDetails()
        {
            base.GetDetails();
            Console.WriteLine("Enter contractDuration");
            contractDuration = Console.ReadLine();
            Console.WriteLine("enter project");
            project = Console.ReadLine();
            Console.WriteLine("Enter chargesPerDay");
            chargesPerDay = Int32.Parse(Console.ReadLine());
        }
        //public void PartTimeDisplayDetails()
        public override void DisplayDetails()
        {
            base.DisplayDetails();
            Console.WriteLine($"contractDuration is {contractDuration}");
            Console.WriteLine($"project is {project}");
            Console.WriteLine($"chargesPerDay are {chargesPerDay}");
        }
    }
}
