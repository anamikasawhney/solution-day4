﻿using System.ComponentModel.DataAnnotations;

namespace InheritanceDemo
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //Employee employee;

            //Console.WriteLine("PartTimeEmp");

            //employee = new PartTimeEmployee();
            //employee.GetDetails();
            //employee.DisplayDetails();

            Console.WriteLine("Enter what type of employee need to add");
            string empType = Console.ReadLine();
            Employee emp;
            if (empType.Equals("pt"))
            {
                emp = new PartTimeEmployee();
            }
            else
                emp = new FullTimeEmployee();


        }
    }
}
