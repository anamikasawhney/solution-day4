﻿namespace AbstratClass_Interface
{
    abstract class Shape
    {
        protected float area;
        public abstract void GetDimensions();
        public abstract void CalculateArea();
        public void DisplayArea()
        {
            Console.WriteLine("Area is " + area);
        }
    }
    class Square : Shape
    {
        int side;
        
        public override void GetDimensions()
        {
            Console.WriteLine("Enter side value");
            side = byte.Parse(Console.ReadLine());
        }
        public override void CalculateArea()
        {
            area = side * side;
        }
       
    }

    class Rectangle : Shape
    {
        int length, width;
        
        public override void GetDimensions()
        {
            Console.WriteLine("Enter length value");
            length = byte.Parse(Console.ReadLine());

            Console.WriteLine("Enter width value");
            width = byte.Parse(Console.ReadLine());
        }
        public override void CalculateArea()
        {
            area = length * width;
        }
        
    }
    class Triangle : Shape
    {
        int Base, height;
        public override void CalculateArea()
        {
            area = (float).5 * Base
                 * height;
        }

        public override void GetDimensions()
        {
            Console.WriteLine("Enter Base value");
            Base = byte.Parse(Console.ReadLine());

            Console.WriteLine("Enter height value");
            height = byte.Parse(Console.ReadLine());
        }
    }


    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("which shape");
            int shapeType = byte.Parse(Console.ReadLine());
            Shape shape=null;
            switch(shapeType)
            {
                case 1: shape = new Square(); break;
                case 2: shape = new Rectangle(); break;
                case 3: shape = new Triangle(); break;
            }
            shape.GetDimensions();
            shape.CalculateArea();
            shape.DisplayArea();
        }
    }
}
